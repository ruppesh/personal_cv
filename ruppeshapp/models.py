from django.db import models
from django.utils import timezone


class TimeStamp(models.Model):
    created_at = models.DateTimeField(auto_now_add=True, null=True)
    updated_at = models.DateTimeField(auto_now=True, null=True, blank=True)
    deleted_at = models.DateTimeField(null=True, blank=True)

    class Meta:
        abstract = True

    def delete(self):
        self.deleted_at = timezone.now()
        super().save()




class Organization(TimeStamp):
    name = models.CharField(max_length=255)
    established_date = models.DateTimeField(
        auto_now_add=True, null=True, blank=True)
    phone1 = models.CharField(max_length=20, null=True, blank=True)
    phone2 = models.CharField(max_length=20, null=True, blank=True)
    email = models.CharField(max_length=50, null=True, blank=True)
    location = models.CharField(max_length=500)
    image = models.ImageField(upload_to='orgimage/', null=True, blank=True)
    logo = models.ImageField(upload_to="logo/")
    description = models.TextField()
    slogan = models.CharField(max_length=1024, null=True, blank=True)

    def __str__(self):
        return self.name



class Message(TimeStamp):
    name = models.CharField(max_length=255)
    email = models.EmailField()
    phone = models.PositiveIntegerField(null=True, blank=True)
    subject = models.CharField(max_length=200)
    message = models.TextField()

    def __str__(self):
        return self.name