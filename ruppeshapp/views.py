from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth import authenticate, login, logout
from django.core.urlresolvers import reverse_lazy
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.views.generic import *
from .models import *
from .forms import *


class AdminLogout(LoginRequiredMixin, View):
    def get(self, request):
        logout(request)
        return HttpResponseRedirect('/')


class AdminLogin(FormView):
    template_name = "ruppeshapp/admintemplates/adminlogin.html"
    form_class = AdminLoginForm
    success_url = '/school-admin/'

    def form_valid(self, form):
        username = form.cleaned_data['username']
        password = form.cleaned_data['password']
        user = authenticate(username=username, password=password)
        if user is not None and user.is_superuser:
            login(self.request, user)
        else:
            return render(self.request, self.template_name, {
                'form': form,
                'errors': 'Incorrect username or password'})
        return super().form_valid(form)





class AdminMixin(LoginRequiredMixin, object):
    login_url = '/school-admin/login/'

    def get_context_data(self, *args, **kwargs):
        context = super(AdminMixin, self).get_context_data(*args, **kwargs)
        context['organization'] = Organization.objects.get(id=1)
        return context


class DeleteMixin(LoginRequiredMixin, UpdateView):
    login_url = '/school-admin/login/'

    def form_valid(self, form):
        form.instance.deleted_at = timezone.now()
        form.save()
        return super().form_valid(form)

class AdminHomeView(AdminMixin, TemplateView):
    template_name = 'ruppeshapp/admintemplates/adminhome.html'

class AdminOrganizationCreateView(AdminMixin, CreateView):
    template_name = 'ruppeshapp/admintemplates/adminorganizationcreate.html'
    form_class = AdminOrganizationCreateForm
    success_url = reverse_lazy('ruppeshapp:adminhome')


class AdminOrganizationUpdateView(AdminMixin, UpdateView):
    template_name = 'ruppeshapp/admintemplates/adminorganizationcreate.html'
    model = Organization
    form_class = AdminOrganizationCreateForm
    success_url = reverse_lazy('ruppeshapp:adminhome')


    # client


class ClientMixin(object):
    def get_context_data(self, *args, **kwargs):
        context = super(ClientMixin, self).get_context_data(*args, **kwargs)
        context['organization'] = Organization.objects.get(id=1)
        


class ClientHomeView(ClientMixin, CreateView):
    template_name='ruppeshapp/clienttemplates/clienthome.html'
    form_class = MessageForm
    success_url = '/'