from django import forms
from .models import *



class DateTimeInput(forms.DateInput):
    input_type = 'time'


class AdminLoginForm(forms.Form):
    username = forms.CharField(
        widget=forms.TextInput(
            attrs={'class': 'form-control'}))
    password = forms.CharField(
        widget=forms.PasswordInput(
            attrs={'class': 'form-control'}))



class AdminOrganizationCreateForm(forms.ModelForm):
    class Meta:
        model = Organization
        exclude = ['created_at', 'updated_at', 'deleted_at']

    def __init__(self, *args, **kwargs):
        super(AdminOrganizationCreateForm, self).__init__(*args, **kwargs)
        for field in iter(self.fields):
            self.fields[field].widget.attrs.update(
                {'class': 'form-control'})



class MessageForm(forms.ModelForm):
    name = forms.CharField(
        widget=forms.TextInput(
            attrs={'class': 'form-control', 'placeholder': 'Write Your Name.'
            }))
    phone = forms.CharField(
        widget=forms.TextInput(
            attrs={'class': 'form-control', 'placeholder': 'Write Your Phone Number.'
            }))
    subject = forms.CharField(
        widget=forms.TextInput(
            attrs={'class': 'form-control', 'placeholder': 'Write Your Subject.'
            }))
    email = forms.CharField(
        widget=forms.EmailInput(
            attrs={'class': 'form-control', 'placeholder': 'Write Your Email.'
            }))
    message = forms.CharField(
        widget=forms.Textarea(
            attrs={'class': 'form-control', 'placeholder': 'Write Your message.'
            }))
    class Meta:
        model = Message
        fields = ['name', 'phone', 'email', 'subject', 'message']

    def __init__(self, *args, **kwargs):
        super(MessageForm, self).__init__(*args, **kwargs)
        for field in iter(self.fields):
            self.fields[field].widget.attrs.update(
                {'class': 'form-control'})


class AdminMessageDeleteForm(forms.ModelForm):

    class Meta:
        model = Message
        fields = ['deleted_at']
        widgets = {
            'deleted_at': forms.HiddenInput(),
        }