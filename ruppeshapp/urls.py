from django.conf.urls import url 
from .views import *


urlpatterns = [

    url(r'^school-admin/logout/$', AdminLogout.as_view(),
        name='adminlogout'),
    url(r'^school-admin/login/$', AdminLogin.as_view(),
        name='adminlogin'),




    url(r'^school-admin/$', AdminHomeView.as_view(), name='adminhome'),
    url(r'^school-admin/school/create/$',
        AdminOrganizationCreateView.as_view(), name='adminorganizationcreate'),
    url(r'^school-admin/school/(?P<pk>\d+)/update/$',
        AdminOrganizationUpdateView.as_view(), name='adminorganizationupdate'),


    # client
    url(r'^$', ClientHomeView.as_view(), name='clienthome'),
    

       ]